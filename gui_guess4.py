from tkinter import *
from tkinter import messagebox
import time
import random

root = Tk()
root.geometry('200x210+300+300')
root.title("Guess4")

var = StringVar()
label = Label(root, textvariable=var)
label.grid(row=0, column=0, columnspan=4)
var.set("請輸入一個4位不重複數字")
#新數字輸入
def updatenum(buttonString):
    content = num.get()
    num.set(content + buttonString)
    if len(content) >= 4:
        num.set('')
        var.set("輸入錯誤！請重新輸入!")

#清除現有輸入
def clear():
    num.set('')

#新回合
def NewRound():
    start = time.perf_counter()
    global ans
    digit = ('0123456789')
    ans = ''.join(random.sample(digit, 4))
    num.set('')
    var.set('遊戲已重置')

digit = ('0123456789')
ans = ''.join(random.sample(digit, 4))
#打印現有答案
print(ans)
num = StringVar()
E = Entry(root,width = 25,textvariable = num)
E.grid(row = 1, column = 0, columnspan = 4)
k = num.set('')

t = Text(root,width = 25)
t.grid(row= 8 ,column = 0, columnspan = 8)

#按下guess進行數字
def guess():
    word = E.get()
    print (word)
    A = 0
    B = 0
    if len(word) == 4:
        pass
    else:
        clear()
        var.set("輸入錯誤！請重新輸入!")
    for i in range(4):
        if word[i] == ans[i]:
            A += 1
        elif word[i] in ans:
            B += 1
    var.set("%sA%sB" % (A, B))
    result = word + ' ' + str(A) + 'A' + str(B) + 'B'
    t.insert('end', result + '\n')
    if word == ans:
        var.set('你答對了')
        messagebox.showinfo(title='Congraualation', message='You win the game')
        pass

#GUI
Button7 = Button(root, text = '7', width = 8, command = lambda:updatenum('7')).grid(row = 3, column = 0)
Button8 = Button(root, text = '8', width = 8, command = lambda:updatenum('8')).grid(row = 3, column = 1)
Button9 = Button(root, text = '9', width = 8, command = lambda:updatenum('9')).grid(row = 3, column = 2)
Button4 = Button(root, text = '4', width = 8, command = lambda:updatenum('4')).grid(row = 4, column = 0)
Button5 = Button(root, text = '5', width = 8, command = lambda:updatenum('5')).grid(row = 4, column = 1)
Button6 = Button(root, text = '6', width = 8, command = lambda:updatenum('6')).grid(row = 4, column = 2)
Button1 = Button(root, text = '1', width = 8, command = lambda:updatenum('1')).grid(row = 5, column = 0)
Button2 = Button(root, text = '2', width = 8, command = lambda:updatenum('2')).grid(row = 5, column = 1)
Button3 = Button(root, text = '3', width = 8, command = lambda:updatenum('3')).grid(row = 5, column = 2)
Button0 = Button(root, text = '0', width = 26, command = lambda:updatenum('0')).grid(row = 6, column = 0, columnspan = 3)
ButtonN = Button(root, text = 'NewRound', width = 8, command = NewRound).grid(row = 7, column = 0)
ButtonC = Button(root, text = 'c',width = 8, command = clear).grid(row=7,column=1)
ButtonG = Button(root, text = 'Guess', width = 8, command = lambda:guess()).grid(row = 7, column = 2)

root.mainloop()