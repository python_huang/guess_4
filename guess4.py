import random
count = 0
digit = ('0123456789')
ans = ''.join(random.sample(digit, 4))
word = ''
print("*" * 5 + "猜數字遊戲" + "*" * 5)
while 1:
    count += 1
    a, b = 0, 0 # 0A0B
    while 1:
        word = input("請輸入一個4位數數字:")
        if len(word) == 4:
            break
        else:
            print("輸入錯誤！請重新輸入!")
    for i in range(4):
        if word[i] == ans[i]:
            a += 1
        elif word[i] in ans:
            b += 1
    print("%sA%sB" % (a, b))
    if word == ans:
        print("你答對了，總共輸入了%s次" % count)
        break